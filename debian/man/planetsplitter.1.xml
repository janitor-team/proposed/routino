<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='planetsplitter'>

  <refmeta>
    <refentrytitle>planetsplitter</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>planetsplitter</refname>
    <refpurpose>split OSM XML data into routino database</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>planetsplitter</command>
      <arg choice='opt'><option>--help</option></arg>
      <arg choice='opt'><option>--dir=<replaceable>dirname</replaceable></option></arg>
      <arg choice='opt'><option>--prefix=<replaceable>name</replaceable></option></arg>
      <arg choice='opt'><option>--sort-ram-size=<replaceable>size</replaceable></option></arg>
      <arg choice='opt'><option>--sort-threads=<replaceable>number</replaceable></option></arg>
      <arg choice='opt'><option>--tmpdir=<replaceable>dirname</replaceable></option></arg>
      <arg choice='opt'><option>--tagging=<replaceable>filename</replaceable></option></arg>
      <arg choice='opt'><option>--loggable</option></arg>
      <arg choice='opt'><option>--logtime</option></arg>
      <arg choice='opt'><option>--logmemory</option></arg>
      <arg choice='opt'><option>--errorlog<arg choice='opt'>=<replaceable>name</replaceable></arg></option></arg>
      <group>
        <arg choice='opt'><option>--parse-only</option></arg>
        <arg choice='opt'><option>--process-only</option></arg>
      </group>
      <arg choice='opt'><option>--append</option></arg>
      <arg choice='opt'><option>--keep</option></arg>
      <arg choice='opt'><option>--changes</option></arg>
      <arg choice='opt'><option>--max-iterations=<replaceable>number</replaceable></option></arg>
      <arg choice='opt'><option>--prune-none</option></arg>
      <arg choice='opt'><option>--prune-isolated=<replaceable>len</replaceable></option></arg>
      <arg choice='opt'><option>--prune-short=<replaceable>len</replaceable></option></arg>
      <arg choice='opt'><option>--prune-straight=<replaceable>len</replaceable></option></arg>
      <group>
        <arg choice='opt'><replaceable>filename.osm</replaceable> ...</arg>
        <arg choice='opt'><replaceable>filename.osc</replaceable> ...</arg>
        <arg choice='opt'><replaceable>filename.pbf</replaceable> ...</arg>
        <arg choice='opt'><replaceable>filename.o5m</replaceable> ...</arg>
        <arg choice='opt'><replaceable>filename.o5c</replaceable> ...</arg>
        <arg choice='opt'><replaceable>filename.(o5m|osc|os5m|o5c).bz2</replaceable> ...</arg>
        <arg choice='opt'><replaceable>filename.(o5m|osc|os5m|o5c).gz</replaceable> ...</arg>
        <arg choice='opt'><replaceable>filename.(o5m|osc|os5m|o5c).xz</replaceable> ...</arg>
      </group>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>planetsplitter</command> reads in the OSM format XML file and
      splits it up to create the <command>routino</command> database that is
      for routing.
    </para>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>--help</option></term>
        <listitem>
          <para>
            Prints usage information
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--dir</option>=<replaceable>dirname</replaceable></term>
        <listitem>
          <para>
            Sets the directory name in which to save the results.
            Defaults to the current directory.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--prefix</option>=<replaceable>name</replaceable></term>
        <listitem>
          <para>
            Sets the filename prefix for the files that are created.
            Defaults to no prefix.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--sort-ram-size</option>=<replaceable>size</replaceable></term>
        <listitem>
          <para>
            Specifies the amount of RAM (in MB) to use for sorting the data.
            If not specified then 64 MB will be used in slim mode or 256 MB
            otherwise.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--sort-threads</option>=<replaceable>number</replaceable></term>
        <listitem>
          <para>
            The number of threads to use for data sorting (the sorting memory
            is shared between the threads \- too many threads and not enough
            memory will reduce the performance).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--tmpdir</option>=<replaceable>dirname</replaceable></term>
        <listitem>
          <para>
            Specifies the name of the directory to store the temporary disk
            files.
            If not specified then it defaults to either the value of the
            <option>--dir</option> option or the current directory.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--tagging</option>=<replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Sets the filename containing the list of tagging rules in XML
            format for the parsing the input files.
            If the file doesn't exist then
           <replaceable>dirname</replaceable>,
           <replaceable>prefix</replaceable> and "tagging.xml" will be
           combined and used, if that doesn't exist then the file
           <filename>/usr/share/routino/tagging.xml</filename>
           will be used.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--loggable</option></term>
        <listitem>
          <para>
            Print progress messages that are suitable for logging to a file;
            normally an incrementing counter is printed which is more suitable
            for real-time display than logging.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--logtime</option>></term>
        <listitem>
          <para>
            Print the elapsed time for each processing step
            (minutes, seconds and milliseconds).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--logmemory</option></term>
        <listitem>
          <para>
            Print the maximum allocated and mapped memory for each processing
            step (MBytes).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--errorlog[=<replaceable>name</replaceable>]</option></term>
        <listitem>
          <para>
            Log OSM parsing and processing errors to
            <filename>error.log</filename> or the specified file name
            (the <option>--dir</option> and <option>--prefix</option> options
            are applied).
            If the <option>--append</option> option is used then the existing
            log file will be appended, otherwise a new one will be created.
            If the <option>--keep</option> option is also used a geographically
            searchable database of error logs is created for use in the
            visualiser.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--parse-only</option></term>
        <listitem>
          <para>
            Parse the input files and store the data in intermediate files but
            don't process the data into a routing database.
            This option must be used with the <option>--append</option> option
            for all except the first file.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--process-only</option></term>
        <listitem>
          <para>
            Don't read in any files but process the existing intermediate files
            created by using the <option>--parse-only</option> option.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--append</option></term>
        <listitem>
          <para>
            Parse the input file and append the result to the existing
            intermediate files; the appended file can be either an OSM
            file or an OSC change file.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--keep</option></term>
        <listitem>
          <para>
            Store a set of intermediate files after parsing the OSM files,
            sorting and removing duplicates; this allows appending an OSC
            file and re-processing later.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--changes</option></term>
        <listitem>
          <para>
            This option indicates that the data being processed contains one or
            more OSC (OSM changes) files, they must be applied in time sequence
            if more than one is used.
            This option implies <option>--append</option> when parsing data
            files and <option>--keep</option> when processing data.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--max-iterations</option>=<replaceable>number</replaceable></term>
        <listitem>
          <para>
            The maximum number of iterations to use when generating super-nodes
            and super-segments.
            Defaults to 5 which is normally enough.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--prune-none</option></term>
        <listitem>
          <para>
            Disable the prune options below, they can be re-enabled by adding
            them to the command line after this option.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--prune-isolated</option>=<replaceable>length</replaceable></term>
        <listitem>
          <para>
            Remove the access permissions for a transport type from small
            disconnected groups of segments and remove the segments if they
            end up with no access permission (defaults to removing groups
            under 500m).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--prune-short</option>=<replaceable>length</replaceable></term>
        <listitem>
          <para>
            Remove short segments (defaults to removing segments up to a
            maximum length of 5m).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--prune-straight=</option>=<replaceable>length</replaceable></term>
        <listitem>
          <para>
            Remove nodes in almost straight highways (defaults to removing
            nodes up to 3m offset from a straight line).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><filename>filename.osm</filename></term>
        <term><filename>filename.osc</filename></term>
        <term><filename>filename.pbf</filename></term>
        <term><filename>filename.o5m</filename></term>
        <term><filename>filename.o5c</filename></term>
        <listitem>
          <para>
            Specifies the filename(s) to read data from.
            Filenames ending '.pbf' will be read as PBF, filenames ending in
            '.o5m' or '.o5c' will be read as O5M/O5C, otherwise as XML.
            Filenames ending '.bz2' will be bzip2 uncompressed
            (if bzip2 support compiled in).
            Filenames ending '.gz' will be gzip uncompressed
            (if gzip support compiled in).
            Filenames ending '.xz' will be xz uncompressed
            (if xz support compiled in).
          </para>
          <para>
            Note: In version 2.5 of Routino the ability to read data from the
            standard input has been removed.
            This is because there is now the ability to read compressed files
            (bzip2, gzip, xz) and PBF files directly.
            Also using standard input the file type cannot be auto-detected
            from the filename.
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

  </refsect1>

  <refsect1 id='examples'>
    <title>EXAMPLES</title>

    <example>
      <para>
        Example usage 1:
      </para>
      <screen>
<command>planetsplitter</command> <option>--dir</option>=<filename>data</filename> <option>--prefix</option>=gb <filename>great_britain.osm</filename>
      </screen>
      <para>
        This will generate the output files
        <filename>data/gb-nodes.mem</filename>,
        <filename>data/gb-segments.mem</filename> and
        <filename>data/gb-ways.mem</filename>.
        Multiple filenames can be specified on the command line and they will
        all be read in, combined and processed together.
      </para>
    </example>

    <example>
      <para>
        Example usage 2:
      </para>
      <screen>
<command>planetsplitter</command> <option>--dir</option>=data <option>--prefix</option>=gb <option>--parse-only</option>          <filename>great_britain_part1.osm</filename>
<command>planetsplitter</command> <option>--dir</option>=data <option>--prefix</option>=gb <option>--parse-only</option> <option>--append</option> <filename>great_britain_part2.osm</filename>
<command>planetsplitter</command> <option>--dir</option>=data <option>--prefix</option>=gb <option>--parse-only</option> <option>--append</option> ...
<command>planetsplitter</command> <option>--dir</option>=data <option>--prefix</option>=gb <option>--process-only</option>
      </screen>
      <para>
        This will generate the same output files as the first example but
        parsing the input files is performed separately from the data
        processing.
        The first file read in must not use the <option>--append</option>
        option but the later ones must.
      </para>
    </example>

    <example>
      <para>
        Example usage 3:
      </para>
      <screen>
<command>planetsplitter</command> <option>--dir</option>=<filename>data</filename> <option>--prefix</option>=gb <option>--keep</option>    <filename>great_britain.osm</filename>

<command>planetsplitter</command> <option>--dir</option>=<filename>data</filename> <option>--prefix</option>=gb <option>--changes</option> <filename>great_britain.osc</filename>
      </screen>
      <para>
        This will generate the same output files as the first example.
        The first command will process the complete file and keep some
        intermediate data for later.
        The second command will apply a set of changes to the stored
        intermediate data and keep the updated intermediate files for
        repeating this step later with more change data.
      </para>
      <para>
        The parsing and processing can be split into multiple commands
        as it was in example 2 with the <option>--keep</option> option
        used with <option>--process-only</option> for the initial OSM
        file(s) and the <option>--changes</option> option used with
        <option>--parse-only</option> or <option>--process-only</option>
        for every OSC file.
      </para>
    </example>

  </refsect1>

</refentry>
