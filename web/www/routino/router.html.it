<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="openstreetmap routing route planner">
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, user-scalable=no">

<title>Routino : Pianificazione percorso per i dati di OpenStreetMap</title>

<!--
Routino router web page.

Part of the Routino routing software.

This file Copyright 2008-2018 Andrew M. Bishop

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see http://www.gnu.org/licenses/.
-->

<!-- Page elements -->
<script src="page-elements.js" type="text/javascript"></script>
<link href="page-elements.css" type="text/css" rel="stylesheet">

<!-- Router and visualiser shared features -->
<link href="maplayout.css" type="text/css" rel="stylesheet">

<!-- Router specific features -->
<script src="profiles.js" type="text/javascript"></script>
<link href="router.css" type="text/css" rel="stylesheet">

<!-- Map parameters -->
<script src="mapprops.js" type="text/javascript"></script>

<!-- Map loader -->
<script src="maploader.js" type="text/javascript"></script>

</head>
<body onload="map_load('html_init();map_init();form_init();');">

<!-- Left hand side of window - data panel -->

<div class="left_panel">

<div class="tab_box">
<span id="tab_options" onclick="tab_select('options');" class="tab_selected" title="Imposta le opzioni del percorso">Opzioni</span>
<span id="tab_results" onclick="tab_select('results');" class="tab_unselected" title="Vedi i risultati del percorso">Risultati</span>
<span id="tab_data" onclick="tab_select('data');" class="tab_unselected" title="Informazioni sulla base dati">Dati</span>
</div>

<div class="tab_content" id="tab_options_div">

<form name="form" id="form" action="#" method="get" onsubmit="return false;">
<div class="hideshow_box">
<span class="hideshow_title">Routino OpenStreetMap Router</span>
Questa pagina consente il calcolo del percorso con i dati raccolti da OpenStreetMap.
Seleziona i punti di partenza e di arrivo (clicca sulle icone dei marcatori qui sotto), seleziona le preferenze e trova un percorso.
<div class="center">
<a target="other" href="http://www.routino.org/">Pagina web di Routino</a>
|
<a target="other" href="documentation/">Documentazione</a>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_language_show" onclick="hideshow_show('language');" class="hideshow_show">+</span>
<span id="hideshow_language_hide" onclick="hideshow_hide('language');" class="hideshow_hide">-</span>
<span class="hideshow_title">Lingua</span>

<div id="hideshow_language_div" style="display: none;">
<table>
<tr>
<td><a id="lang_en_url" href="router.html.en" title="English language webpage">English</a>
<td>(EN)
<td><input name="language" type="radio" value="en" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_cs_url" href="router.html.cs" title="Stránka v češtině">Česky</a>
<td>(CS)
<td><input name="language" type="radio" value="cs" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_de_url" href="router.html.de" title="Deutsche Webseite">Deutsch</a>
<td>(DE)
<td><input name="language" type="radio" value="de" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_es_url" href="router.html.es" title="Español webpage">Español</a>
<td>(ES)
<td><input name="language" type="radio" value="es" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_fi_url" href="router.html.fi" title="Suomi webpage">Suomi</a>
<td>(FI)
<td><input name="language" type="radio" value="fi" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_fr_url" href="router.html.fr" title="Page web en français">Français</a>
<td>(FR)
<td><input name="language" type="radio" value="fr" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_hu_url" href="router.html.hu" title="Magyar weboldal">Magyar</a>
<td>(HU)
<td><input name="language" type="radio" value="hu" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_it_url" href="router.html.it" title="Italiano webpage">Italiano</a>
<td>(IT)
<td><input name="language" type="radio" value="it" onchange="formSetLanguage();" checked>
<tr>
<td><a id="lang_nl_url" href="router.html.nl" title="Nederlandse web pagina">Nederlands</a>
<td>(NL)
<td><input name="language" type="radio" value="nl" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_pl_url" href="router.html.pl" title="Polski webpage">Polski</a>
<td>(PL)
<td><input name="language" type="radio" value="pl" onchange="formSetLanguage();" >
<tr>
<td><a id="lang_ru_url" href="router.html.ru" title="Страница на русском языке">Русский</a>
<td>(RU)
<td><input name="language" type="radio" value="ru" onchange="formSetLanguage();" >
</table>
<a target="translation" href="http://www.routino.org/translations/">Routino Translations</a>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_waypoint_show" onclick="hideshow_show('waypoint');" class="hideshow_hide">+</span>
<span id="hideshow_waypoint_hide" onclick="hideshow_hide('waypoint');" class="hideshow_show">-</span>
<span class="hideshow_title">Punti sul percorso</span>
<div id="hideshow_waypoint_div">
<div id="waypoints">
<div id="waypointXXX" class="waypoint" style="display: none;">
<img id="iconXXX" class="waypoint-icon" src="icons/marker-XXX-grey.png" title="Posizione del punto XXX" alt="Waypoint XXX" onmouseup="markerToggleMap(XXX);" draggable="true">
<span id="coordsXXX" style="display: none;">
<input name="lonXXX" type="text" size="6" title="Longitudine del punto XXX" onchange="formSetCoords(XXX);">E
<input name="latXXX" type="text" size="7" title="Latitudine del punto XXX" onchange="formSetCoords(XXX);">N
</span>
<span id="searchXXX">
<input name="searchXXX" type="text" size="18" title="Luogo del punto XXX"> <!-- uses Javascript event for triggering -->
</span>
<div class="waypoint-buttons" style="display: inline-block;">
<img alt="?" src="icons/waypoint-search.png" title="Ricerca del luogo" onmousedown="markerSearch(XXX);" >
<img alt="G" src="icons/waypoint-locate.png" title="Ottieni luogo attuale" onmousedown="markerLocate(XXX);" >
<img alt="O" src="icons/waypoint-recentre.png" title="Centra la mappa su questo punto" onmousedown="markerRecentre(XXX);">
<img alt="^" src="icons/waypoint-up.png" title="Muovi in alto questo punto" onmousedown="markerMoveUp(XXX);" >
<img alt="+" src="icons/waypoint-add.png" title="Aggiungi un punto dopo questo" onmousedown="markerAddAfter(XXX);">
<br>
<img alt="#" src="icons/waypoint-coords.png" title="Coordinate per la posizione" onmousedown="markerCoords(XXX);" >
<img alt="~" src="icons/waypoint-home.png" title="Imposta come casa" onmousedown="markerHome(XXX);" >
<img alt="o" src="icons/waypoint-centre.png" title="Centra questo punto sulla mappa" onmousedown="markerCentre(XXX);" >
<img alt="v" src="icons/waypoint-down.png" title="Muovi in basso questo punto" onmousedown="markerMoveDown(XXX);">
<img alt="-" src="icons/waypoint-remove.png" title="Rimuovi questo punto dal percorso" onmousedown="markerRemove(XXX);" >
</div>
<div id="searchresultsXXX" style="display: none;">
</div>
</div>
</div>
<div id="waypoints-buttons">
<table>
<tr><td>Chiudi il ciclo: <td><input type="checkbox" name="loop" onchange="formSetLoopReverse('loop' );">
<tr><td>Ordine inverso:<td><input type="checkbox" name="reverse" onchange="formSetLoopReverse('reverse');">
</table>
<div class="waypoint-buttons" style="display: inline-block;">
<img src="icons/waypoint-loop.png" title="Aggiungi un nuovo punto per chiudere il ciclo" onmousedown="markersLoop();">
<img src="icons/waypoint-reverse.png" title="Ordine inverso dei punti sul percorso" onmousedown="markersReverse();">
</div>
</div>
</div>
</div>

<div class="hideshow_box">
<span class="hideshow_title">Trova</span>
<input type="button" title="Trova il percorso più breve" id="shortest1" value="Percorso piu' breve" onclick="findRoute('shortest');" disabled="disabled">
<input type="button" title="Trova il percorso più veloce" id="quickest1" value="Percorso piu' veloce" onclick="findRoute('quickest');" disabled="disabled">
</div>

<div class="hideshow_box">
<span id="hideshow_transport_show" onclick="hideshow_show('transport');" class="hideshow_hide">+</span>
<span id="hideshow_transport_hide" onclick="hideshow_hide('transport');" class="hideshow_show">-</span>
<span class="hideshow_title">Tipo di trasporto</span>
<div id="hideshow_transport_div">
<table>
<tr><td>A piedi: <td><input name="transport" type="radio" value="foot" onchange="formSetTransport('foot' );">
<tr><td>Cavallo: <td><input name="transport" type="radio" value="horse" onchange="formSetTransport('horse' );">
<tr><td>Carrozzella:<td><input name="transport" type="radio" value="wheelchair" onchange="formSetTransport('wheelchair');">
<tr><td>Bicicletta: <td><input name="transport" type="radio" value="bicycle" onchange="formSetTransport('bicycle' );">
<tr><td>Ciclomotore: <td><input name="transport" type="radio" value="moped" onchange="formSetTransport('moped' );">
<tr><td>Motociclo:<td><input name="transport" type="radio" value="motorcycle" onchange="formSetTransport('motorcycle');">
<tr><td>Autovettura: <td><input name="transport" type="radio" value="motorcar" onchange="formSetTransport('motorcar' );">
<tr><td>Merce: <td><input name="transport" type="radio" value="goods" onchange="formSetTransport('goods' );">
<tr><td>Trasporto Merci Pesante: <td><input name="transport" type="radio" value="hgv" onchange="formSetTransport('hgv' );">
<tr><td>Mezzi Pubblici: <td><input name="transport" type="radio" value="psv" onchange="formSetTransport('psv' );">
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_highway_show" onclick="hideshow_show('highway');" class="hideshow_show">+</span>
<span id="hideshow_highway_hide" onclick="hideshow_hide('highway');" class="hideshow_hide">-</span>
<span class="hideshow_title">Preferenze per autostrada</span>
<div id="hideshow_highway_div" style="display: none;">
<table>
<tr><td>Autostrada: <td><input name="highway-motorway" type="text" size="3" onchange="formSetHighway('motorway' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('motorway' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('motorway' ,'+');">
<tr><td>Superstrada: <td><input name="highway-trunk" type="text" size="3" onchange="formSetHighway('trunk' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('trunk' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('trunk' ,'+');">
<tr><td>Statale: <td><input name="highway-primary" type="text" size="3" onchange="formSetHighway('primary' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('primary' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('primary' ,'+');">
<tr><td>Regionale: <td><input name="highway-secondary" type="text" size="3" onchange="formSetHighway('secondary' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('secondary' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('secondary' ,'+');">
<tr><td>Provinciale: <td><input name="highway-tertiary" type="text" size="3" onchange="formSetHighway('tertiary' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('tertiary' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('tertiary' ,'+');">
<tr><td>Non classificata:<td><input name="highway-unclassified" type="text" size="3" onchange="formSetHighway('unclassified','=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('unclassified','-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('unclassified','+');">
<tr><td>Residenziale: <td><input name="highway-residential" type="text" size="3" onchange="formSetHighway('residential' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('residential' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('residential' ,'+');">
<tr><td>Servizio: <td><input name="highway-service" type="text" size="3" onchange="formSetHighway('service' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('service' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('service' ,'+');">
<tr><td>Traccia: <td><input name="highway-track" type="text" size="3" onchange="formSetHighway('track' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('track' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('track' ,'+');">
<tr><td>Pista ciclabile: <td><input name="highway-cycleway" type="text" size="3" onchange="formSetHighway('cycleway' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('cycleway' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('cycleway' ,'+');">
<tr><td>Sentiero: <td><input name="highway-path" type="text" size="3" onchange="formSetHighway('path' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('path' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('path' ,'+');">
<tr><td>Passaggi: <td><input name="highway-steps" type="text" size="3" onchange="formSetHighway('steps' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('steps' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('steps' ,'+');">
<tr><td>Traghetto: <td><input name="highway-ferry" type="text" size="3" onchange="formSetHighway('ferry' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetHighway('ferry' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetHighway('ferry' ,'+');">
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_speed_show" onclick="hideshow_show('speed');" class="hideshow_show">+</span>
<span id="hideshow_speed_hide" onclick="hideshow_hide('speed');" class="hideshow_hide">-</span>
<span class="hideshow_title">Limiti di velocita'</span>
<div id="hideshow_speed_div" style="display: none;">
<table>
<tr><td>Autostrada: <td><input name="speed-motorway" type="text" size="3" onchange="formSetSpeed('motorway' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('motorway' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('motorway' ,'+');">
<tr><td>Superstrada: <td><input name="speed-trunk" type="text" size="3" onchange="formSetSpeed('trunk' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('trunk' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('trunk' ,'+');">
<tr><td>Statale: <td><input name="speed-primary" type="text" size="3" onchange="formSetSpeed('primary' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('primary' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('primary' ,'+');">
<tr><td>Regionale: <td><input name="speed-secondary" type="text" size="3" onchange="formSetSpeed('secondary' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('secondary' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('secondary' ,'+');">
<tr><td>Provinciale: <td><input name="speed-tertiary" type="text" size="3" onchange="formSetSpeed('tertiary' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('tertiary' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('tertiary' ,'+');">
<tr><td>Non classificata:<td><input name="speed-unclassified" type="text" size="3" onchange="formSetSpeed('unclassified','=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('unclassified','-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('unclassified','+');">
<tr><td>Residenziale: <td><input name="speed-residential" type="text" size="3" onchange="formSetSpeed('residential' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('residential' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('residential' ,'+');">
<tr><td>Servizio: <td><input name="speed-service" type="text" size="3" onchange="formSetSpeed('service' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('service' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('service' ,'+');">
<tr><td>Traccia: <td><input name="speed-track" type="text" size="3" onchange="formSetSpeed('track' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('track' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('track' ,'+');">
<tr><td>Pista ciclabile: <td><input name="speed-cycleway" type="text" size="3" onchange="formSetSpeed('cycleway' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('cycleway' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('cycleway' ,'+');">
<tr><td>Sentiero: <td><input name="speed-path" type="text" size="3" onchange="formSetSpeed('path' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('path' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('path' ,'+');">
<tr><td>Passaggi: <td><input name="speed-steps" type="text" size="3" onchange="formSetSpeed('steps' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('steps' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('steps' ,'+');">
<tr><td>Traghetto: <td><input name="speed-ferry" type="text" size="3" onchange="formSetSpeed('ferry' ,'=');"><td>km/hr<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetSpeed('ferry' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetSpeed('ferry' ,'+');">
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_property_show" onclick="hideshow_show('property');" class="hideshow_show">+</span>
<span id="hideshow_property_hide" onclick="hideshow_hide('property');" class="hideshow_hide">-</span>
<span class="hideshow_title">Impostazione proprieta'</span>
<div id="hideshow_property_div" style="display: none;">
<table>
<tr><td>Asfaltata: <td><input name="property-paved" type="text" size="3" onchange="formSetProperty('paved' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetProperty('paved' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetProperty('paved' ,'+');">
<tr><td>Corsie multiple: <td><input name="property-multilane" type="text" size="3" onchange="formSetProperty('multilane' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetProperty('multilane' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetProperty('multilane' ,'+');">
<tr><td>Ponte: <td><input name="property-bridge" type="text" size="3" onchange="formSetProperty('bridge' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetProperty('bridge' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetProperty('bridge' ,'+');">
<tr><td>Tunnel: <td><input name="property-tunnel" type="text" size="3" onchange="formSetProperty('tunnel' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetProperty('tunnel' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetProperty('tunnel' ,'+');">
<tr><td>Percorso pedonale:<td><input name="property-footroute" type="text" size="3" onchange="formSetProperty('footroute' ,'=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetProperty('footroute' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetProperty('footroute' ,'+');">
<tr><td>Percorso ciclabile:<td><input name="property-bicycleroute" type="text" size="3" onchange="formSetProperty('bicycleroute','=');"><td>%<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetProperty('bicycleroute','-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetProperty('bicycleroute','+');">
</table>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_restriction_show" onclick="hideshow_show('restriction');" class="hideshow_show">+</span>
<span id="hideshow_restriction_hide" onclick="hideshow_hide('restriction');" class="hideshow_hide">-</span>
<span class="hideshow_title">Altri limitazioni</span>
<div id="hideshow_restriction_div" style="display: none;">
<table>
<tr><td>Strade a senso unico:<td><input name="restrict-oneway" type="checkbox" onchange="formSetRestriction('oneway');">
<tr><td>Divieto di svolta: <td><input name="restrict-turns" type="checkbox" onchange="formSetRestriction('turns' );">
</table>
<table>
<tr><td>Peso:<td><input name="restrict-weight" type="text" size="3" onchange="formSetRestriction('weight','=');"><td>tonnes<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetRestriction('weight','-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetRestriction('weight','+');">
<tr><td>Altezza:<td><input name="restrict-height" type="text" size="3" onchange="formSetRestriction('height','=');"><td>metres<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetRestriction('height','-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetRestriction('height','+');">
<tr><td>Larghezza: <td><input name="restrict-width" type="text" size="3" onchange="formSetRestriction('width' ,'=');"><td>metres<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetRestriction('width' ,'-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetRestriction('width' ,'+');">
<tr><td>Lunghezza:<td><input name="restrict-length" type="text" size="3" onchange="formSetRestriction('length','=');"><td>metres<td><img alt="<" src="icons/waypoint-left.png" title="-" onmousedown="formSetRestriction('length','-');">&ndash;/+<img alt=">" src="icons/waypoint-right.png" title="+" onmousedown="formSetRestriction('length','+');">
</table>
</div>
</div>

<div class="hideshow_box">
<span class="hideshow_title">Collegamenti</span>
<a id="permalink_url" href="router.html">Collegamento a questa vista della mappa</a>
<br>
<a id="edit_url" target="edit" style="display: none;">Modifica questi dati OSM</a>
</div>

<div class="hideshow_box">
<span id="hideshow_help_options_show" onclick="hideshow_show('help_options');" class="hideshow_hide">+</span>
<span id="hideshow_help_options_hide" onclick="hideshow_hide('help_options');" class="hideshow_show">-</span>
<span class="hideshow_title">Aiuto</span>
<div id="hideshow_help_options_div">
<div class="scrollable">
<B> Avvio rapido </ b>
<br>
Seleziona le icone dei marcatori (sopra) per posizionarli sulla mappa (a destra). Poi
Trascinarli nella posizione desiderata. Eventualmente ingrandire la mappa per
posizionarli con maggiore precisione. In alternativa digitare la latitudine e
longitudine nelle apposite caselle.
<P>
Selezionare il tipo di trasporto, i tipi di strade e le loro proprietà, i limiti di velocità
e altre impstazioni dalle opzioni sopra riportate.
Selezionare il tipo di percorso "più breve" o "più veloce" per calcolarlo e visualizzarlo sulla mappa.
<P>
<B> Punti sul percorso </ b>
<br>
Selezionando le icone dei marcatori, questi saranno visualizzati sulla mappa.
Quando viene calcolato un percorso, ogni punto viene visualizzato nel modo più
coerente possibile al tipo di trasporto selezionato, ciascuno nell'ordine indicato.
<P>
<B> Tipo di trasporto </ b>
<br>
La selezione di un tipo di trasporto vincola il tipo di tragitto ed imposta i
valori predefiniti per gli altri parametri relativi.
<P>
<B> Preferenze per tipi di strade </ b>
<br>
La percorrenza sulle strade principali sono impostate come percentuale di preferenza.
Ad esempio, se ad una strada primaria viene data una preferenza del 100%,
mentre ad una secondaria una preferenza del 90%, significa che il percorso sulla strada primaria
può essere fino al 10% più lungo di quello sulla strada secondaria.
<P>
<B> Limiti di velocità </ b>
<br>
I limiti di velocità per i vari tipi di strade si applicano se la strada stessa
non ha altri limiti di velocità contrassegnati o è superiore a quello scelto.
<P>
<B> Preferenze sulla proprietà </ b>
<br>
La preferenza sulla proprietà viene impostata come percentuale e vengono scelti
i tragitti cercando di rispettare tali preferenze.
Ad esempio, se ad una strada asfaltata viene data una preferenza del 75% allora automaticamente
ad una non asfaltata viene data una preferenza del 25%. In questo modo un percorso su una
strada asfaltata può risultare filo a 3 volte più lungo rispetto a quella non asfaltata.
<P>
<B> Altre restrizioni </ b>
<br>
Si può decidere di ignorare i limiti imposti come peso, altezza, larghezza o lunghezza.
È anche possibile ignorare restrizioni di senso unico se per esempio si desidera
un percorso da fare a piedi.
</div>
</div>
</div>
</form>
</div>


<div class="tab_content" id="tab_results_div" style="display: none;">

<div class="hideshow_box">
<span class="hideshow_title">Stato</span>
<div id="result_status">
<div id="result_status_not_run">
<b><i>Percorso bloccato</i></b>
</div>
<div id="result_status_running" style="display: none;">
<b>Percorso in esecuzione</b>
</div>
<div id="result_status_complete" style="display: none;">
<b>Percorso completo</b>
<br>
<a id="router_log_complete" target="router_log" href="#">Vedi dettagli</a>
</div>
<div id="result_status_error" style="display: none;">
<b>Errore di percorso</b>
<br>
<a id="router_log_error" target="router_log" href="#">Vedi dettagli</a>
</div>
<div id="result_status_failed" style="display: none;">
<b>Errore di esecuzione del percorso</b>
</div>
</div>
</div>

<div class="hideshow_box">
<span class="hideshow_title">Trova</span>
<input type="button" title="Trova il percorso più breve" id="shortest2" value="Percorso piu' breve" onclick="findRoute('shortest');" disabled="disabled">
<input type="button" title="Trova il percorso più veloce" id="quickest2" value="Percorso piu' veloce" onclick="findRoute('quickest');" disabled="disabled">
</div>

<div class="hideshow_box">
<span id="hideshow_shortest_show" onclick="hideshow_show('shortest');" class="hideshow_show">+</span>
<span id="hideshow_shortest_hide" onclick="hideshow_hide('shortest');" class="hideshow_hide">-</span>
<span class="hideshow_title">Percorso piu' breve</span>
<div id="shortest_status">
<div id="shortest_status_no_info">
<b><i>Nessuna informazione</i></b>
</div>
<div id="shortest_status_info" style="display: none;">
</div>
</div>
<div id="hideshow_shortest_div" style="display: none;">
<div id="shortest_links" style="display: none;">
<table>
<tr><td>Direzioni in formato HTML: <td><a id="shortest_html" target="shortest_html" href="#">Nuova pagine</a>
<tr><td>File con tracciato GPX: <td><a id="shortest_gpx_track" target="shortest_gpx_track" href="#">Nuova pagine</a>
<tr><td>File con percorso GPX: <td><a id="shortest_gpx_route" target="shortest_gpx_route" href="#">Nuova pagine</a>
<tr><td>File di testo con piu' informazioni: <td><a id="shortest_text_all" target="shortest_text_all" href="#">Nuova pagine</a>
<tr><td>File di testo: <td><a id="shortest_text" target="shortest_text" href="#">Nuova pagine</a>
</table>
<hr>
</div>
<div id="shortest_route">
</div>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_quickest_show" onclick="hideshow_show('quickest');" class="hideshow_show">+</span>
<span id="hideshow_quickest_hide" onclick="hideshow_hide('quickest');" class="hideshow_hide">-</span>
<span class="hideshow_title">Percorso piu' veloce</span>
<div id="quickest_status">
<div id="quickest_status_no_info">
<b><i>Nessuna informazione</i></b>
</div>
<div id="quickest_status_info" style="display: none;">
</div>
</div>
<div id="hideshow_quickest_div" style="display: none;">
<div id="quickest_links" style="display: none;">
<table>
<tr><td>Direzioni in formato HTML: <td><a id="quickest_html" target="quickest_html" href="#">Nuova pagine</a>
<tr><td>File con tracciato GPX: <td><a id="quickest_gpx_track" target="quickest_gpx_track" href="#">Nuova pagine</a>
<tr><td>File con percorso GPX: <td><a id="quickest_gpx_route" target="quickest_gpx_route" href="#">Nuova pagine</a>
<tr><td>File di testo con piu' informazioni: <td><a id="quickest_text_all" target="quickest_text_all" href="#">Nuova pagine</a>
<tr><td>File di testo: <td><a id="quickest_text" target="quickest_text" href="#">Nuova pagine</a>
</table>
<hr>
</div>
<div id="quickest_route">
</div>
</div>
</div>

<div class="hideshow_box">
<span id="hideshow_help_route_show" onclick="hideshow_show('help_route');" class="hideshow_hide">+</span>
<span id="hideshow_help_route_hide" onclick="hideshow_hide('help_route');" class="hideshow_show">-</span>
<span class="hideshow_title">Aiuto</span>
<div id="hideshow_help_route_div">
<div class="scrollable">
<B> Avvio rapido </ b>
<br>
Dopo aver calcolato un percorso è possibile scaricare il file GPX o la descrizione
del percorso in formato testo (riassuntivo o versione dettagliata).
Inoltre si può visualizzare la descrizione del percorso e ingrandire le parti selezionate.
<P>
<B> Risoluzione dei problemi </ b>
<br>
Se il calcolo termina con un errore, la causa più probabile è
che non sia possibile trovare un percorso tra i punti selezionati.
Spostare uno o più marcatori o cambiare le opzioni di routing dovrebbe
consente di risolvere il problema.
<P>
<B> Formati generati </ b>
<br>
<Dl>
<Dt> istruzioni HTML
<Dd> Una descrizione del percorso con indicazioni per ciascun punto importante.
<Dt> file di traccia GPX
<Dd> Le stesse informazioni visualizzate sulla mappa con i punti
per ogni nodo e linee per ogni segmento.
<Dt> file di percorso GPX
<Dd> Le stesse informazioni visualizzate nel testo per ogni punto importante sul percorso.
<Dt> File di testo completo
<Dd> Un elenco di tutti i nodi visitati con la distanza
tra loro e la distanza cumulativa per ogni passo del percorso.
<Dt> File di testo
<Dd> Le stesse informazioni sul percorso visualizzate nel testo.
</ Dl>
</div>
</div>
</div>
</div>


<div class="tab_content" id="tab_data_div" style="display: none;">
<div class="hideshow_box">
<span class="hideshow_title">Statistiche Routio</span>
<div id="statistics_data"></div>
<a id="statistics_link" href="statistics.cgi" onclick="displayStatistics();return(false);">Mostra statistiche dei dati</a>
</div>

<div class="hideshow_box">
<span class="hideshow_title">Visualizzatore Routino</span>
Per visualizzare i dati di Routino esiste un visualizzatore che consente di mostrarli in vari modi.
<br>
<a id="visualiser_url" href="visualiser.html" target="visualiser">Collegamento a questa vista della mappa</a>
</div>
</div>

</div>

<!-- Right hand side of window - map -->

<div class="right_panel">
<div class="map" id="map">
<noscript>
<p>
E' <em>necessario</em> Javascript per questa pagina web in quanto la mappa è interattiva.
</noscript>
</div>
<div class="attribution">
Percorso: <a href="http://www.routino.org/" target="routino">Routino</a>
|
Dati Geografici: <span id="attribution_data"></span>
|
Tiles: <span id="attribution_tile"></span>
</div>
</div>

</body>

</html>
